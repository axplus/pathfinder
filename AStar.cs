using System;
using System.Collections.Generic;
// using Terrain;


namespace Terrain {
        public class NoWay : Exception {}
        public static class AStar {
                public static List<Terrain.Point> AStarAlgorithm(Terrain.Bitmap bmp,
                                                                 Terrain.Point from_, Terrain.Point to) {
                        List<Terrain.Point> path = AStar_(bmp, from_, to);
                        AStarO1(path);
                        return path;
                }

                private static float Distance(Terrain.Point pt1, Terrain.Point pt2) {
                        return (float)Math.Sqrt(Math.Pow((double)(pt1.x-pt2.x), 2) +
                                                Math.Pow((double)(pt1.y-pt2.y), 2));
                }

                private static float GetPrice(Terrain.Bitmap bmp, Terrain.Point pt, Terrain.Point target) {
                        if (bmp[pt] == 1) {
                                return float.MaxValue;
                        }
                        return Distance(pt, target);
                }

                private static int[] Range(int from_, int to)
                {
                        List<int> r = new List<int>();
                        for (int i = from_; i < to; ++i) {
                                r.Add(i);
                        }
                        return r.ToArray();
                }

                private static List<Terrain.Point> Children(Terrain.Bitmap bmp, Terrain.Point pt) {
                        List<Terrain.Point> r = new List<Terrain.Point>();
                        foreach (int xx in Range(Math.Max(pt.x-1, 0), Math.Min(pt.x+2, bmp.Width))) {
                                foreach (int yy in Range(Math.Max(pt.y-1, 0), Math.Min(pt.y+2, bmp.Height))) {
                                        if (pt.x == xx && pt.y == yy) {
                                                continue;
                                        }
                                        if (bmp[xx,yy] == 1) {
                                                continue;
                                        }
                                        r.Add(new Terrain.Point(xx, yy));
                                }
                        }
                        return r; // .ToArray();
                }

                private static List<Terrain.Point> AStar_(Terrain.Bitmap bmp, Terrain.Point from, Terrain.Point to)
                {
						if (bmp[from] == 1)
				from = FindFreePt(from, bmp, to);
                        if (bmp[to] == 1)
                        {
                            to = FindFreePt(to, bmp, from); //    throw new NoWay();
                        }
                        List<Terrain.Point> paths = new List<Terrain.Point>();
                        Terrain.Point nextstep = from;
                        Dictionary<Terrain.Point, bool> close_ = new Dictionary<Terrain.Point, bool>();
                        paths.Add(from);
                        while (!nextstep.Equals(to))
                        {
                                close_[nextstep] = true;
                                List<Terrain.Point> childsteps = Children(bmp, nextstep);
                                childsteps.Sort(delegate (Terrain.Point pt1, Terrain.Point pt2) {
                                                float p1 = GetPrice(bmp, pt1, to);
                                                float p2 = GetPrice(bmp, pt2, to);
                                                if (p1 > p2) return 1;
                                                if (p1 == p2) return 0;
                                                return -1;
                                        } );
                                List<Terrain.Point> open_ = new List<Terrain.Point>();
                                foreach (var pt in childsteps) {
                                        if (!close_.ContainsKey(pt)) {
                                                open_.Add(pt);
                                        }
                                }
                                try {
                                        nextstep = open_[0];
                                        paths.Add(nextstep);
                                } catch (ArgumentOutOfRangeException) {
                                        paths.RemoveAt(paths.Count - 1);
                                        try {
                                                nextstep = paths[paths.Count - 1];
                                        } catch (ArgumentOutOfRangeException) {
                                                throw new NoWay();
                                        }
                                }
                        }
                        paths.Add(to);
                        return paths;
                }

                private static void AStarO1(List<Terrain.Point> path) {
                        int i = 0;
                        while (i < path.Count) {
                                bool found = false;
                                foreach (int j in Range(i+2, path.Count)) {
                                        if (Distance(path[i], path[j]) <= Math.Sqrt(2)) {
                                                path.RemoveRange(i+1, j-i-1);
                                                found = true;
                                                break;
                                        }
                                }
                                if (!found) {
                                        i += 1;
                                }
                        }
                }
                private static List<Terrain.Point> GetRing(Terrain.Point pt, int level, Terrain.Bitmap bmp)
                {
                        List<Terrain.Point> r = new List<Terrain.Point>();
                        foreach (var xx in Range(Math.Max(pt.x-level, 0), Math.Min(pt.x+level+1, bmp.Width))) {
                                foreach (var yy in Range(Math.Max(pt.y-level, 0), Math.Min(pt.y+level+1, bmp.Height))) {
                                        if (pt.x == xx && pt.y == yy)
                                                continue;
                                        r.Add(new Point(xx, yy));
                                }
                        }
                        return r;
                }
                private static Terrain.Point FindFreePt(Terrain.Point pt, Terrain.Bitmap bmp, Terrain.Point ref_) {
                        List<Terrain.Point> points = GetRing(pt, 3, bmp);
                        List<Terrain.Point> av = new List<Terrain.Point>();
                        foreach (var p in points) {
                                if (bmp[p] == 0)
                                        av.Add(p);
                        }
                        if (av.Count == 1) {
                                return av[0];
                        }
                        if (av.Count > 0) {
                                av.Sort(delegate(Terrain.Point pt1, Terrain.Point pt2) {
                                                var d1 = Distance(pt1, ref_);
                                                var d2 = Distance(pt2, ref_);
                                                if (d1 == d2)
                                                        return 0;
                                                if (d1 > d2)
                                                        return 1;
                                                return 0;
                                        });
                                return av[0];
                        }
                        throw new NoWay();
                }
        }
}
