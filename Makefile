timestamp=$(shell date +"%Y%m%d%H%M%S")
monoprefix:=/Applications/Unity/MonoDevelop.app/Contents/Frameworks/Mono.framework/Versions/2.10.2/bin
mono:=$(monoprefix)/mono
xbuild:=$(monoprefix)/xbuild


.PHONY: all
all: bin/Debug/terrainreader.exe


.PHONY: commit
commit:
	git commit -a -m '$(timestamp)'


.PHONY: clean
clean:
	$(RM) -rd bin obj


.PHONY: run
run: bin/Debug/terrainreader.exe
	$(mono) --debug "$<"


bin/Debug/terrainreader.exe::
	$(xbuild) terrainreader.sln
