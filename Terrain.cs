using System;
using System.Collections.Generic;
//using PointList = List<Point>;


namespace Terrain {


        public class Point : object {
                public int x, y;

                public Point(int x_, int y_) {x = x_; y = y_;}

                public override bool Equals(object other_) {
                        Point other = (Point)other_;
                        return x == other.x && y == other.y;
                }

                public override int GetHashCode () {
                        return x * 10000 + y;
                }

                public override string ToString() {
                        return string.Format("{0},{1}", x, y);
                }

        }


        public struct Bitmap {
		        public static List<Terrain.Point> FindProcess(string terrainfile, Terrain.Point from_, Terrain.Point to) {
                        Terrain.TerrainFile tfile = new Terrain.TerrainFile();
                        // useReal是什么意思呢？
                        tfile.LoadTerrainFile(terrainfile/*"map_191.terrain"*/, /*useReal=*/true);
                        Terrain.Bitmap bmp = tfile.ToBitmap();
                        List<Terrain.Point> path =
                                Terrain.AStar.AStarAlgorithm(bmp, from_, to); // new Terrain.Point(1, 48), new Terrain.Point(87, 58));
                        return path;
                }

                public Bitmap(int width, int height) {
                        _width = width;
                        _height = height;
                        _m = new ushort[width, height];
                }

                public int Width { get { return _width; }}

                public int Height { get {return _height;}}

                public ushort this[int x, int y] {
                        get {return _m[x,y];}
                        set {_m[x,y] = value;}
                }

                public ushort this[Point pt] {
                        get {return this[pt.x, pt.y];}
                        set {this[pt.x, pt.y] = value;}
                }

                public void Dump() {
                        for (int y = 0; y < _height; ++y) {
                                for (int x = 0; x < _width; ++x) {
                                        Console.Write(_m[x,y]);
                                        Console.Write(" ");
                                }
                                Console.WriteLine();
                        }
                }

		public void Mark(List<Point> path, ushort value) {
			foreach (Point pt in path) {
				this[pt] = value;
			}
		}

                int _width, _height;
                ushort[,] _m;
        }
}

