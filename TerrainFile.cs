﻿// TerrainManager.cs
// Created by ershu  2007-10-27
// 地形功能管理部分

using System;
using System.Collections.Generic;
//using System.Text;
using System.Collections;
//using System.Drawing;
//using System.Drawing.Imaging;
using System.IO;
using System.Diagnostics;


namespace Terrain {

        public class LoadFailedException : Exception {
                public LoadFailedException(string errorMessage) : base(errorMessage) {}
        }

        public class TerrainFile {
                // id : tnode 映射表
                //private Hashtable mHashTable = new Hashtable();

                // 地形层/高度层/NPC层的数据
                public ushort[,] mTerrainLayer    = null;
                public ushort[,] mHeightLayer     = null;

                // 地形层的宽、高(格子坐标)
                public int mWidthInGrid = 0;
                public int mHeightInGrid = 0;

                // 地图的id
                public int mMapId = -1;


                // default constructor
                public TerrainFile() {
                }


                // 一个完整的查找过程
                public static List<Terrain.Point> FindProcess(string terrainfile, Terrain.Point from_, Terrain.Point to) {
                        Terrain.TerrainFile tfile = new Terrain.TerrainFile();
                        // useReal是什么意思呢？
                        tfile.LoadTerrainFile(terrainfile/*"map_191.terrain"*/, /*useReal=*/true);
                        Terrain.Bitmap bmp = tfile.ToBitmap();
                        List<Terrain.Point> path =
                                Terrain.AStar.AStarAlgorithm(bmp, from_, to); // new Terrain.Point(1, 48), new Terrain.Point(87, 58));
                        return path;
                }

                // 接口：载入一个指定的地形文件
                public void LoadTerrainFile(string filePath, bool useReal) {
                        System.IO.FileStream fs = System.IO.File.Open(filePath, System.IO.FileMode.Open);
                        System.IO.BinaryReader binrayreader = new System.IO.BinaryReader(fs);
                        try {

                                // 读取magicNo
                                UInt32 magicNo = binrayreader.ReadUInt32();
                                if (magicNo != 0x494d5345 && magicNo != 0x4a4d5345) {
                                        throw new LoadFailedException(filePath + "不是有效的地形文件");
                                }

                                // 读取id
                                mMapId = (int)System.Net.IPAddress.NetworkToHostOrder(binrayreader.ReadInt32());
                                //mMapId = mEditorCore.MapId;
                                mWidthInGrid = (int)System.Net.IPAddress.NetworkToHostOrder(binrayreader.ReadInt32());
                                mHeightInGrid = (int)System.Net.IPAddress.NetworkToHostOrder(binrayreader.ReadInt32());

                                // 分配内存
                                mHeightLayer = new ushort[mHeightInGrid, mWidthInGrid];
                                mTerrainLayer = new ushort[mHeightInGrid, mWidthInGrid];

                                // 定义一个临时变量保存高度信息或者地形信息
                                ushort mTempLayer = 0;

                                // 读取高度信息
                                for (int j = 0; j < mHeightInGrid; j++)
                                        for (int i = 0; i < mWidthInGrid; i++)
                                        {
                                                mTempLayer = binrayreader.ReadUInt16();

                                                // 如果高度为1000的则认为是NPC障碍点的高度信息
                                                // 应该先清掉，后面会根据NPC.csv表重新生成最新的
                                                // NPC障碍点信息
                                                if (mTempLayer == 1000 && (! useReal))
                                                        mHeightLayer[j, i] = 0;
                                                else
                                                        mHeightLayer[j, i] = mTempLayer;
                                        }

                                // 读取地形信息
                                for (int j = 0; j < mHeightInGrid; j++)
                                        for (int i = 0; i < mWidthInGrid; i++)
                                        {
                                                mTempLayer = binrayreader.ReadUInt16();

                                                // 如果terrain_id为1的则认为是NPC障碍点的terrain_id
                                                // 应该先清掉，后面会根据NPC.csv表重新生成最新的
                                                // NPC障碍点信息
                                                if (mTempLayer == 1 && (! useReal))
                                                        mTerrainLayer[j, i] = 0xffff;
                                                else
                                                        mTerrainLayer[j, i] = mTempLayer;
                                        }

                                // 关闭掉流和文件
                        } finally {
                                binrayreader.Close();
                                fs.Close();
                        }
                }

                public Terrain.Bitmap ToBitmap() {
                        int rows = mHeightLayer.GetLength(0);
                        int columns = mHeightLayer.GetLength(1);
                        Terrain.Bitmap bmp = new Terrain.Bitmap(columns, rows);
                        for (int x = 0; x < columns; ++x)
                        {
                                for (int y = 0; y < rows; ++y)
                                {
                                        bmp[x,y] = (ushort)(mHeightLayer[y,x] > 0 ? 1 : 0);
                                }
                        }
                        return bmp;
                }
        }
}
