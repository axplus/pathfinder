# encoding: utf-8


import os
import sys
import math
import random
import time


class NoWay(Exception): pass


def astar(bmp, from_, to):
    path = _astar(bmp, from_, to)
    _astaro1(path)
    return path


def distance(pt1, pt2):
    x1, y1 = pt1
    x2, y2 = pt2
    return math.sqrt((x1-x2)**2 + (y1-y2)**2)


def getprice(bmp, pt, target):
    '''
    求的是到目标的最近距离，而不是到地图右下角的
    '''
    if bmp[pt] == 1:
        return sys.maxint
    return distance(pt, target)


def children(bmp, pt):
    x, y = pt
    width, height = bmp.size
    r = []
    # 因为是半开区间，所以要+2
    for xx in range(max(x-1, 0), min(x+2, width)):
        for yy in range(max(y-1, 0), min(y+2, height)):
            if (xx,yy) == pt:
                # 跳过自己
                continue
            if bmp[xx,yy] == 1:
                # 跳过障碍点
                continue
            r.append((xx,yy))
    return r


def getring(pt, level, bmp):
    x,y = pt
    width, height = bmp.size
    r = []
    for xx in range(max(x-level, 0), min(x+level+1, width)):
        for yy in range(max(y-level, 0), min(y+level+1, height)):
            if (xx,yy) == pt:
                continue
            r.append((xx,yy))
    return r


def findfreept(pt, bmp, ref):
    # 直接获得3层辐射的所有点
    points = getring(pt, 3, bmp)
    # print points ####
    # 抛掉其中障碍点
    av = [p for p in points if bmp[p] == 0]
    # 如果只有一点，就是他了
    # print points, '->', av ####
    if len(av) == 1:
        return av[0]
    # 如果有多个点，按距离排序
    if av:
        av.sort(key=lambda p: distance(p, ref))
        return av[0]
    # 实在没有的话，给出异常
    raise NoWay()


def _astar(bmp, from_, to):
    '''一个完整的A*查找过程'''
    # 站在一个障碍点上或者目标是一个障碍点
    if bmp[from_] == 1:
        # TODO: 修正from_，查找距离from_最近的一个非障碍点
        from_ = findfreept(from_, bmp, to)
    if bmp[to] == 1:
        # 修正to
        to = findfreept(to, bmp, from_)
    paths = []
    nextstep = from_
    close_ = set()
    paths.append(from_)
    while nextstep != to:
        close_.add(nextstep)
        childsteps = children(bmp, nextstep)
        childsteps.sort(key=lambda pt: getprice(bmp, pt, to))
        # 对childsteps进行排序
        open_ = [pt for pt in childsteps
                 # 排除掉已经走过的路
                 if pt not in close_]
        try:
            nextstep = open_[0]
            paths.append(nextstep)
        except IndexError:
            # 无路可走了
            # 退回上一步
            paths.pop()
            try:
                nextstep = paths[-1]
            except IndexError:
                # 没有可以走的路了
                raise NoWay()
    paths.append(to)
    return paths


def _astaro1(path):
    '''
    对astar的原始结果进行一次优化，这次优化的只是将路径中不必要的迂回干
    掉
    '''
    i = 0
    while i < len(path):
        # 不处理紧接其后的那个点
        found = False
        for j in range(i+2, len(path)):
            if distance(path[i], path[j]) <= math.sqrt(2):
                # 从path中干掉(i,j)即[i+1,j)
                del path[i+1:j]
                found = True
                break
        if not found:
            i += 1
