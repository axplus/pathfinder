# encoding: utf-8
#
# 这个测试载入地图，从地图的最左上角的位置寻路到地图最右下角的位置


import sys
from astar import astar, NoWay
from terrain import readterrain


mapfile = sys.argv[1]


bmp = readterrain(mapfile)
if '191' in mapfile:
    from_, to = (38,25), (50,20)
else:
    width, height = bmp.size
    from_, to = (0, 0), (width-1, height-1)
print mapfile, 'map size', '=', bmp.size, 'from', from_, 'to', to
try:
    path = astar(bmp, from_, to)
except NoWay:
    path = [from_, to]
    pass

bmp.mark(path, '*')
bmp.dump()
