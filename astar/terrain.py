# encoding: utf-8


import random
from matrix import Bitmap


def generatemap(width, height):
    bmp = Bitmap(width, height)
    for x in range(width):
        for y in range(height):
            r = 0 if random.random() < 0.8 else 1
            bmp[x,y] = r
    return bmp


def readterrain(terrainfile):
    '''
    读取一个.pterrain文件，这个文件的格式采用1和0的文本表示，用空格隔开
    '''
    arrays = []
    with file(terrainfile) as in_:
        for line in in_:
            sline = line.strip()
            row = []
            for ch in sline.split(' '):
                try:
                    row.append(int(ch))
                except ValueError:
                    row.append(ch)
            arrays.append(row)
    # 进行一次行列变换
    width, height = len(arrays[0]), len(arrays)
    bmp = Bitmap(width, height)
    for x in range(width):
        for y in range(height):
            bmp[x,y] = arrays[y][x]
    return bmp
