# encoding: utf-8
#


import unittest
import os
import time
import random

from astar import astar, NoWay
from terrain import readterrain, generatemap


class TestAstar(unittest.TestCase):
    '''这个测使用用例'''

    def testAstarMap(self):
        for mapid in range(10):
            filename = str(mapid)
            if os.path.isfile(filename):
                bmp = readterrain(filename)
                width, height = bmp.size
                try:
                    path = astar(bmp, (0,0), (width-1,height-1))
                except NoWay:
                    pass
