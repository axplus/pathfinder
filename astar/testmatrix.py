import unittest
from terrain import readterrain
from matrix import Bitmap, Point


class TestBitmap(unittest.TestCase):
    def testBasic(self):
        bmp = Bitmap(1024, 768)
        self.assertEqual(bmp[0,0], 0)
        self.assertEqual(bmp.size, (1024, 768))

    def testDump(self):
        bmp = Bitmap(10, 10)
        bmp[5,5] = 1
        self.assertEqual(bmp[5,4], 0)
        bmp.dump()

    def testEqual(self):
        bmp1 = Bitmap(10, 10)
        bmp2 = Bitmap(10, 10)
        self.assertEqual(bmp1, bmp2)
        bmp1[4,4] = 10
        bmp2[4,4] = 10
        self.assertEqual(bmp1, bmp2)

    def testMark(self):
        bmp1 = Bitmap(10, 10)
        path = [(0,0), (1,0), (2,0)]
        bmp1.mark(path, '*')
        self.assertEqual(bmp1[1,0], '*')

    def testMatrixOp(self):
        pt1 = Point(10,20)
        pt2 = Point(20,5)
        # +
        self.assertEqual(pt1+pt2, Point(30,25))
        # -
        self.assertEqual(pt1-pt2, Point(-10,15))
        self.assertEqual(pt1-Point(10,20), Point(0,0))
        # *
        self.assertEqual(pt1*pt2, Point(200,100))
        # /
        self.assertEqual(pt1/pt2, Point(0.5,4))



class Test_readterrain(unittest.TestCase):
    def test(self):
        bmp = readterrain('1')
        self.assertEqual(bmp[1,2], 1)
