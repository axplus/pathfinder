# encoding: utf-8
#


import unittest
import os
import time
import random

from astar import astar, NoWay
from terrain import readterrain, generatemap


class TestProfiling(unittest.TestCase):

    def testMutiple(self):
        total = 0.0
        for i in range(100):
            t = time.time()
            try:
                bmp = generatemap(300, 200)
                path = astar(bmp, (0,0), (299,199))
            except NoWay:
                pass
            t2 = time.time()
            total += t2-t
            print 300, 'x', 200, '=', t2-t
        print 'average', '=', total/100 ####
