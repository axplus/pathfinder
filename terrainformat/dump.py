# encoding: utf-8


import sys
import struct
from matrix import Bitmap


def main():
    """docstring for main"""
    with open(sys.argv[1], 'rb') as tfile:
        magic, = struct.unpack('i', tfile.read(4)) # 0x494d5345 or 0x4a4d5345
        # mapid, = struct.unpack('>i  ', tfile.read(4)) # mapid
        # width, = struct.unpack('<i', tfile.read(4)) # width
        mapid, width, height = struct.unpack('>iii', tfile.read(12)) # height
        # 然后是width*height*ushort
        print mapid, width, height ####
        print 'filesize =', 4 + 4 + 4 + 4 + 2 * width * height * 2 ####
        grid = Bitmap(width, height)
        for y in xrange(height):
            for x in xrange(width):
                v, = struct.unpack('H', tfile.read(2)) ####
                grid[x,y] = v
        # 之后是width*height*ushort的地形信息
        grid.dump()
        

if __name__ == '__main__':
    main()
