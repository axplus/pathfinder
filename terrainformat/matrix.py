# encoding: utf-8
# matrix.py
# 2011-11-18


from __future__ import division


class _Matrix(object):

    def __init__(self, rows, columns):
        '''create _Matrix with rows x columns'''
        object.__init__(self)
        self._rows = rows
        self._columns = columns
        self._m = [0] * rows * columns


    @property
    def size(self):
        return (self._rows, self._columns)


    def __repr__(self):
        return str(self._m)


    def __getitem__(self, key):
        r,c = key
        return self._m[r*self._columns + c]


    def __setitem__(self, key, v):
        r,c = key
        self._m[r*self._columns + c] = v


    def __eq__(self, other):
        return self._rows == other._rows and \
            self._columns == other._columns and \
            self._m == other._m


class _Vector(_Matrix):
    def __init__(self, *pts):
        _Matrix.__init__(self, 1, len(pts));
        for i in range(len(pts)):
            self[0,i] = pts[i];


    def __add__(self, other):
        r = []
        for i in range(len(self._m)):
            r.append(self._m[i]+other._m[i])
        return _Vector(*r)


    def __sub__(self, other):
        r = []
        for i in range(len(self._m)):
            r.append(self._m[i]-other._m[i])
        return _Vector(*r)


    def __mul__(self, other):
        r = []
        for i in range(len(self._m)):
            r.append(self._m[i]*other._m[i])
        return _Vector(*r)


    def __div__(self, other):
        r = []
        for i in range(len(self._m)):
            r.append(self._m[i]/other._m[i])
        return _Vector(*r)


class Bitmap(_Matrix):
    '''
    Bitmap这个对象的横纵坐标正好和Matrix是相反的，更符合我们处理Bitmap
    图形时候所使用的习惯
    '''
    def __init__(self, width, height):
        # heightXwidth矩阵
        _Matrix.__init__(self, width, height)
        self._width = width
        self._height = height


    @property
    def size(self):
        return (self._width, self._height)


    def mark(self, path, value):
        for x,y in path:
            self[x,y] = value

    def dump(self):
        for y in range(self._height):
            for x in range(self._width):
                print '%3d' % self[x,y],
            print

Point = _Vector
